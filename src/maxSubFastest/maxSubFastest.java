package maxSubFastest;

public class maxSubFastest {
	
	public static int maxSubFastest(int[] array)
    {
        //index variable t
        int t;
        Integer[] M = new Integer[array.length];
        //initial prefix maximum
        M[0] = 0;

        for (t = 1; t < array.length; t++)
        {
            M[t] = max(0, M[t - 1] + array[t]);
        }
        //maximum found so far
        int m = 0;
        for (t = 1; t < array.length; t++)
        {
            m = max(m, M[t]);
        }
        return m;
    }
	public static int max(int a, int b)
    {
        return a > b ? a:b;
    }
}

