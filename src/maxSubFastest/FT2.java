package maxSubFastest;

import java.util.Arrays;
import java.util.Scanner;

public class FT2 {
	public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int a = kb.nextInt();
       
        int[] arr = new int[a];
        
        for(int i=0; i<a; i++)
        {
            arr[i] = kb.nextInt();
        }
        for(int i=0; i<a/2; i++){
            int j = arr[i];
            arr[i] = arr[a-i-1];
            arr[a-i-1]=j;
        }
        System.out.println(Arrays.toString(arr));
    }
}
